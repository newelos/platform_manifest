# NewelOS-N (ANDROID-7.1.1) manifest
### Create dirs, and install soft, libs:
```
sudo su
add-apt-repository ppa:openjdk-r/ppa
apt-get update
apt-get install bison build-essential curl ccache flex lib32ncurses5-dev lib32z1-dev libesd0-dev libncurses5-dev libsdl1.2-dev libxml2 libxml2-utils lzop pngcrush schedtool squashfs-tools xsltproc zip zlib1g-dev git-core make phablet-tools gperf openjdk-8-jdk
exit 
```
### Create newel folder:
```
mkdir ~/newel
cd ~/newel 
```
### GIT config (nickname, e-mail):
```
git config --global user.email "mail@domain.com"
git config --global user.name "login"
```
### To initialize your local repository use:
```
repo init -u https://bitbucket.org/newelos/platform_manifest.git -b n
```
### Then to sync up:
```
repo sync -j4 -f
```